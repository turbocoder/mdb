# MovieDB (MDB)

## Detalles

**Creador:** Yamil García Hernández

**Destino:** [Soluciones GBH](https://gbh.com.do/)

**Motivo:** Prueba Técnica

**Versión de Android minima:** 5.0 (Lollipop) (80% moviles a nivel mundial)

**Versión de Android Studio:** 3.1.3

**Versión de Gradle:** 3.1.3

## Especificaciones

Hacer una aplicación que permita a los usuarios:

- Ver películas (deben ser cacheadas)
- Ver las películas marcadas como favoritas (deben ser persistidas localmente)

Ambos listados de películas deben ser presentados como grids, y deben poder ser organizados por los criterios listados a continuación:

- Nombre
- Año de publicación (predeterminado)
- Rating

La aplicación consistirá de dos pantallas, descritas a continuación:

**Pantalla Principal**

- Nombre de la aplicación
- Filtros
- Grid de películas (como un tab)
- Películas favoritas (como un tab)
        
**Pantalla de detalle**

- Detalle de película, a continuación la estructura de la misma
- Título de la película
- Sipnosis
- Cover de la película
- Rating
- Botón (debe ser un indicador al mismo tiempo) para agregar/remover del listado de favoritas.
        
**Conceptos a tomar en cuenta:**

- SOLID
- Clean Architecture

**Es obligatorio hacer uso de las librerías listadas a continuación:**

- [Retrofit 2](https://github.com/square/retrofit)
- [Dagger 2](https://github.com/google/dagger)
- [Picasso](https://github.com/square/picasso)
- [AutoValue](https://github.com/google/auto)
- [ButterKnife](https://github.com/JakeWharton/butterknife)
- [EventBus](https://github.com/greenrobot/EventBus)
- [Gson](https://github.com/google/gson)
- [Guava](https://github.com/google/guava)
- [RecyclerView](https://developer.android.com/reference/android/support/v7/widget/RecyclerView)

**Fuente de datos:** [TheMovieDB.org](https://www.themoviedb.org/documentation/api)

## Librerias agregadas a conveniencia

- [Iconics](https://github.com/mikepenz/Android-Iconics): Permite agregar iconos utilizando codigos en los textos y asi evitar el uso de imagenes para los mismos.
- [Room](https://developer.android.com/topic/libraries/architecture/room): Permite habilitar con Guava el uso del ORM Room.
- [Auto Value Gson](https://github.com/rharter/auto-value-gson): Permite usar anotaciones para poder transformar un objeto serializado en un modelo estilo auto-value.


## Anotaciones sobre la aplicación

- Me hubiera gustado darle algunos toques al diseño, sin embargo creo que esta decente.
- La aplicación funciona tanto Portrait como Landscape
- No tiene barra de busqueda, ya que no fue especificado en los requerimientos.
- Los filtros de orden son (como no se especifico si eran ascendentes o descendentes se utilizo el más conveniente): 
    - Titulo de forma ascendente
    - Fecha de lanzamiento de forma ascendente
    - Puntuación de forma descendente
- En caso de que una imagen no se encuentre permanecera el palceholder image.
- La aplicación esta unicamente en ingles, los requerimientos no mencionaban el multi-lenguaje.
- No se configuro una versión release con ofuscación, los requerimientos no mencionaban la configuración de dicha versión.
- Se utiliza la cache para guardar datos de la lista y su fuente principal es la base de datos en caso de no encontrar miembros.
- La base de datos unicamente almacena peliculas que estan en el listado de favoritos, para así poder reducir espacio.
- Los listados no estan cacheados, ya que estos estan sujetos a cambios constantes. Lo que si esta cacheado son las peliculas en si.
- La aplicación utiliza Dependency Injection y esta organizado de forma modular.
- La aplicación cuenta con pruebas unitarias y pruebas instrumentadas con [espresso](https://developer.android.com/training/testing/espresso/) para probar el UI.

## Recomendaciones

- Uso de la libreria [AndroidAnnotations](https://github.com/androidannotations/androidannotations) ya que ofrece un set de herramientas mucho mejor que ButterKnife.
- Uso de la libreria [Realm.io](https://realm.io/) en vez de Guava y Room para la cache y base de datos, ya que ofrece una serie de herramientas optimizadas para Mobile, tales como implementaciones Asynchronous por defecto.
- No usar Auto-Value, ya que no cumple con su cometido en Android, al ahorrar tiempo no agregando los setters y getters se pierde el tiempo ajustando para que pueda funcionar con otras librerias importantes tales como Retrofit 2 y Google Room.

### MIT License
    
Copyright (c) [2018] [Yamil Garcia Hernandez - MovieDB - MDB]
    
Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.