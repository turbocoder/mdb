package me.yamilgarciahernandez.mdb.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.cache.Cache;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.MDB;
import me.yamilgarciahernandez.mdb.R;
import me.yamilgarciahernandez.mdb.adapter.MovieListAdapter;
import me.yamilgarciahernandez.mdb.di.component.DaggerMovieListComponent;
import me.yamilgarciahernandez.mdb.listener.EndlessScrollListener;
import me.yamilgarciahernandez.mdb.listener.ServiceListener;
import me.yamilgarciahernandez.mdb.di.module.MovieListModule;
import me.yamilgarciahernandez.mdb.model.Movie;
import me.yamilgarciahernandez.mdb.model.api.MovieListResponse;
import me.yamilgarciahernandez.mdb.network.SortBy;
import me.yamilgarciahernandez.mdb.network.TheMovieDBService;
import me.yamilgarciahernandez.mdb.ui.activity.MovieActivity;
import retrofit2.Call;

public class MovieListFragment extends BaseFragment implements View.OnClickListener, MovieListAdapter.ItemClickListener {
    @BindView(R.id.fragment_movie_list_loading)
    ProgressBar pbLoading;
    @BindView(R.id.fragment_movie_list_rv)
    RecyclerView recyclerView;

    @BindView(R.id.fragment_movie_list_filter_title)
    TextView tvTitleFilter;
    @BindView(R.id.fragment_movie_list_filter_release)
    TextView tvTitleRelease;
    @BindView(R.id.fragment_movie_list_filter_rating)
    TextView tvTitleRating;

    @Inject
    MovieListAdapter adapter;
    @Inject
    GridLayoutManager manager;
    @Inject
    TheMovieDBService service;
    @Inject
    Cache<Integer, Movie> cache;

    private int currentPage = 1; // Actual page
    private int maxPage = 1000; // Limit page based on the api limitations
    private String sort = SortBy.RELEASE_ASC; // Sort by Rating Ascendant

    private EndlessScrollListener endlessScrollListener;

    public static MovieListFragment getInstance() {
        return new MovieListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_movie_list, container, false);
        ButterKnife.bind(this, rootView);
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
        return rootView;
    }

    @Override
    public void inject() {
        DaggerMovieListComponent
                .builder()
                .movieListModule(
                        new MovieListModule(
                                this.getContext(),
                                2
                        )
                )
                .networkModule(((MDB) getContext().getApplicationContext()).getNetworkModule())
                .cacheModule(((MDB) getContext().getApplicationContext()).getCacheModule())
                .build()
                .inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupMovieFilter();
        setupMovieList();
        downloadMovieList(service.getMovieList(BuildConfig.API_KEY, sort, currentPage));
    }

    private void setupMovieFilter() {
        tvTitleFilter.setOnClickListener(this);
        tvTitleRelease.setOnClickListener(this);
        tvTitleRating.setOnClickListener(this);
    }

    private void setupMovieList() {
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
        endlessScrollListener = new EndlessScrollListener(manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                currentPage = page + 1;
                if (currentPage > maxPage) return;
                downloadMovieList(service.getMovieList(BuildConfig.API_KEY, sort, currentPage));
            }
        };
        recyclerView.addOnScrollListener(endlessScrollListener);
    }

    private void downloadMovieList(final Call<MovieListResponse> call) {
        call.enqueue(new ServiceListener<MovieListResponse>(call) {
            @Override
            public void success(MovieListResponse response) throws Exception {
                addMoviesToCache(response.results());
                adapter.addMovies(response.results());
                adapter.notifyDataSetChanged();
                pbLoading.setVisibility(View.GONE);
            }

            @Override
            public void error(Exception ex) {
                Snackbar.make(recyclerView, R.string.error_downloading_movie_list, Snackbar.LENGTH_LONG)
                        .setAction(R.string.retry, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                downloadMovieList(service.getMovieList(BuildConfig.API_KEY, sort, currentPage));
                            }
                        }).show();
            }
        });
    }

    private void addMoviesToCache(ArrayList<Movie> results) {
        for (Movie movie : results) {
            cache.put(movie.id(), movie);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_movie_list_filter_title:
                if (this.sort.equals(SortBy.TITLE_ASC)) return;
                this.sort = SortBy.TITLE_ASC;
                resetList();
                tvTitleFilter.setBackgroundColor(getResources().getColor(R.color.selected_filter));
                tvTitleFilter.setTextColor(getResources().getColor(R.color.md_white_1000));
                break;
            case R.id.fragment_movie_list_filter_release:
                if (this.sort.equals(SortBy.RELEASE_ASC)) return;
                this.sort = SortBy.RELEASE_ASC;
                resetList();
                tvTitleRelease.setBackgroundColor(getResources().getColor(R.color.selected_filter));
                tvTitleRelease.setTextColor(getResources().getColor(R.color.md_white_1000));
                break;
            case R.id.fragment_movie_list_filter_rating:
                if (this.sort.equals(SortBy.RATING_DESC)) return;
                this.sort = SortBy.RATING_DESC;
                resetList();
                tvTitleRating.setBackgroundColor(getResources().getColor(R.color.selected_filter));
                tvTitleRating.setTextColor(getResources().getColor(R.color.md_white_1000));
                break;
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), MovieActivity.class);
        intent.putExtra(MovieActivity.ARG_MOVIE_ID, adapter.getMovie(position).id());
        startActivity(intent);
    }

    public void resetList() {
        currentPage = 1;
        adapter.clearMovies();
        adapter.notifyDataSetChanged();
        endlessScrollListener.resetState();
        pbLoading.setVisibility(View.VISIBLE);
        downloadMovieList(service.getMovieList(BuildConfig.API_KEY, sort, currentPage));
        tvTitleFilter.setBackgroundColor(getResources().getColor(R.color.md_white_1000));
        tvTitleRelease.setBackgroundColor(getResources().getColor(R.color.md_white_1000));
        tvTitleRating.setBackgroundColor(getResources().getColor(R.color.md_white_1000));
        tvTitleFilter.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvTitleRelease.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvTitleRating.setTextColor(getResources().getColor(R.color.colorPrimary));
    }
}