package me.yamilgarciahernandez.mdb.ui.activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.mikepenz.iconics.view.IconicsTextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.R;
import me.yamilgarciahernandez.mdb.adapter.HomePagerAdapter;
import me.yamilgarciahernandez.mdb.di.component.DaggerHomePageComponent;
import me.yamilgarciahernandez.mdb.di.module.HomePageModule;
import me.yamilgarciahernandez.mdb.network.receiver.NetworkStateReceiver;

public class HomeActivity extends BaseActivity {
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_home_tabs)
    TabLayout tabs;

    @BindView(R.id.activity_home_tab_container)
    ViewPager viewPager;

    @Inject
    HomePagerAdapter homePagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setupHomePager();
        EventBus.getDefault().register(this); // register EventBus
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this); // unregister EventBus
    }

    @Override
    protected void inject() {
        DaggerHomePageComponent
                .builder()
                .homePageModule(
                        new HomePageModule(
                                this.getSupportFragmentManager()
                        )
                )
                .build()
                .inject(this);
    }

    // Configuration of views
    private void setupHomePager() {
        // Linking viewpager with tabs
        viewPager.setAdapter(homePagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        tabs.setupWithViewPager(viewPager);

        // Tabs customization
        LinearLayout tabMovies = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.tab_home, null);
        LinearLayout tabFavorites = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.tab_home, null);
        ((IconicsTextView) tabMovies.findViewById(R.id.tab_home_text)).setText("{faw_film} " + getString(R.string.movies));
        ((IconicsTextView) tabFavorites.findViewById(R.id.tab_home_text)).setText("{faw_heart} " + getString(R.string.favorites));
        tabs.getTabAt(0).setCustomView(tabMovies);
        tabs.getTabAt(1).setCustomView(tabFavorites);
    }

    @Subscribe
    public void onEventMainThread(NetworkStateReceiver.NetworkStateChanged event) {
        if (!event.isInternetConnected()) {
            Snackbar.make(toolbar, R.string.no_internet, Snackbar.LENGTH_LONG).show();
        }
    }
}