package me.yamilgarciahernandez.mdb.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.cache.Cache;
import com.squareup.picasso.Picasso;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.BlurTransformation;
import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.MDB;
import me.yamilgarciahernandez.mdb.R;
import me.yamilgarciahernandez.mdb.database.MovieDatabase;
import me.yamilgarciahernandez.mdb.database.MovieWrapper;
import me.yamilgarciahernandez.mdb.di.component.DaggerMovieSingleComponent;
import me.yamilgarciahernandez.mdb.model.Movie;

public class MovieActivity extends BaseActivity implements View.OnClickListener {

    public static final String ARG_MOVIE_ID = "MOVIE_ID";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    MovieDatabase database;
    @Inject
    Cache<Integer, Movie> cache;

    private Movie movie;
    private boolean isFavorite = false;

    // Movie ui elements
    @BindView(R.id.activity_movie_image)
    ImageView ivImage;
    @BindView(R.id.activity_movie_overview)
    TextView tvOverview;
    @BindView(R.id.activity_movie_release)
    TextView tvRelease;
    @BindView(R.id.activity_movie_rating)
    TextView tvRating;

    @BindView(R.id.activity_movie_fab)
    FloatingActionButton fabFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        loadMovie();
        fabFavorite.setOnClickListener(this);
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    @Override
    protected void inject() {
        DaggerMovieSingleComponent
                .builder()
                .databaseModule(((MDB) getApplicationContext()).getDatabaseModule())
                .cacheModule(((MDB) getApplicationContext()).getCacheModule())
                .build()
                .inject(this);
    }

    private void loadMovie() {
        final int id = getIntent().getIntExtra(ARG_MOVIE_ID, 0);
        if (id == 0) {
            showErrorThenFinish(R.string.error_loading_movie);
            return;
        }
        try {
            movie = cache.get(id, new Callable<Movie>() {
                @Override
                public Movie call() throws Exception {
                    try {
                        return MovieWrapper.toMovie(database.movieDao().findById(id));
                    } catch (Exception ex) {
                        return null;
                    }
                }
            });
            if (movie == null) {
                showErrorThenFinish(R.string.error_loading_movie);
                return;
            }

            setTitle(movie.title());
            Picasso.get()
                    .load(BuildConfig.CDN_URL + movie.image())
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .transform(new BlurTransformation(this))
                    .fit()
                    .centerCrop()
                    .into(ivImage);
            tvOverview.setText((movie.overview() != null && !movie.overview().equals("")) ? movie.overview() : getString(R.string.not_defined));
            tvRelease.setText((movie.release() != null && !movie.release().equals("")) ? movie.release() : getString(R.string.not_defined));
            tvRating.setText(String.valueOf(movie.rating()));
            (new AsyncTask<Void, Void, Movie>() {
                @Override
                protected Movie doInBackground(Void... voids) {
                    return MovieWrapper.toMovie(database.movieDao().findById(id));
                }

                @Override
                protected void onPostExecute(Movie movie) {
                    super.onPostExecute(movie);
                    if (movie != null) isFavorite = true;
                    fabFavorite.setImageResource(isFavorite ? R.drawable.ic_filled_heart : R.drawable.ic_outlined_heart);
                }
            }).execute();
        } catch (Exception e) {
            showErrorThenFinish(R.string.error_loading_movie);
        }
    }

    public void showErrorThenFinish(@StringRes int msg) {
        Snackbar.make(toolbar, msg, Snackbar.LENGTH_SHORT).show();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        finish();
                    }
                }, 1000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_movie_fab:
                addOrRemoveFavorite();
                break;
        }
    }

    private void addOrRemoveFavorite() {
        if (isFavorite) {
            (new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    database.movieDao().deleteById(movie.id());
                    return null;
                }
            }).execute();
            Snackbar.make(toolbar, R.string.movie_deleted, Snackbar.LENGTH_SHORT).show();
            isFavorite = false;
        } else {
            (new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    database.movieDao().insertAll(MovieWrapper.fromMovie(movie));
                    return null;
                }
            }).execute();
            Snackbar.make(toolbar, R.string.movie_added, Snackbar.LENGTH_SHORT).show();
            isFavorite = true;
        }
        fabFavorite.setImageResource(isFavorite ? R.drawable.ic_filled_heart : R.drawable.ic_outlined_heart);
    }
}
