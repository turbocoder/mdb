package me.yamilgarciahernandez.mdb.ui.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.common.cache.Cache;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.MDB;
import me.yamilgarciahernandez.mdb.R;
import me.yamilgarciahernandez.mdb.adapter.MovieListAdapter;
import me.yamilgarciahernandez.mdb.database.MovieDatabase;
import me.yamilgarciahernandez.mdb.database.MovieWrapper;
import me.yamilgarciahernandez.mdb.di.component.DaggerFavoriteListComponent;
import me.yamilgarciahernandez.mdb.di.component.DaggerMovieListComponent;
import me.yamilgarciahernandez.mdb.di.module.MovieListModule;
import me.yamilgarciahernandez.mdb.listener.EndlessScrollListener;
import me.yamilgarciahernandez.mdb.listener.ServiceListener;
import me.yamilgarciahernandez.mdb.model.Movie;
import me.yamilgarciahernandez.mdb.model.api.MovieListResponse;
import me.yamilgarciahernandez.mdb.network.SortBy;
import me.yamilgarciahernandez.mdb.network.TheMovieDBService;
import me.yamilgarciahernandez.mdb.ui.activity.MovieActivity;
import retrofit2.Call;

public class FavoriteListFragment extends BaseFragment implements View.OnClickListener, MovieListAdapter.ItemClickListener {
    @BindView(R.id.fragment_movie_list_loading)
    ProgressBar pbLoading;
    @BindView(R.id.fragment_movie_list_not_found)
    TextView tvNotFound;
    @BindView(R.id.fragment_movie_list_rv)
    RecyclerView recyclerView;

    @BindView(R.id.fragment_movie_list_filter_title)
    TextView tvTitleFilter;
    @BindView(R.id.fragment_movie_list_filter_release)
    TextView tvTitleRelease;
    @BindView(R.id.fragment_movie_list_filter_rating)
    TextView tvTitleRating;

    @Inject
    MovieListAdapter adapter;
    @Inject
    GridLayoutManager manager;
    @Inject
    MovieDatabase database;
    @Inject
    Cache<Integer, Movie> cache;

    private String sort = SortBy.RELEASE_ASC; // Sort by Rating Ascendant

    public static FavoriteListFragment getInstance() {
        return new FavoriteListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_movie_list, container, false);
        ButterKnife.bind(this, rootView);
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
        return rootView;
    }

    @Override
    public void inject() {
        DaggerFavoriteListComponent
                .builder()
                .movieListModule(
                        new MovieListModule(
                                this.getContext(),
                                2
                        )
                )
                .databaseModule(((MDB) getContext().getApplicationContext()).getDatabaseModule())
                .cacheModule(((MDB) getContext().getApplicationContext()).getCacheModule())
                .build()
                .inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupMovieFilter();
        setupMovieList();
    }

    @Override
    public void onStart() {
        super.onStart();
        loadMovieList();
    }

    private void setupMovieFilter() {
        tvTitleFilter.setOnClickListener(this);
        tvTitleRelease.setOnClickListener(this);
        tvTitleRating.setOnClickListener(this);
    }

    private void setupMovieList() {
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(manager);
    }

    private void loadMovieList() {
        (new AsyncTask<Void, Void, ArrayList<Movie>>() {
            @Override
            protected ArrayList<Movie> doInBackground(Void... voids) {
                List<MovieWrapper> movies;

                if (sort.equals(SortBy.TITLE_ASC)) {
                    movies = database.movieDao().getAllOrderedByTitle();
                } else if (sort.equals(SortBy.RELEASE_ASC)) {
                    movies = database.movieDao().getAllOrderedByRelease();
                } else if (sort.equals(SortBy.RATING_DESC)) {
                    movies = database.movieDao().getAllOrderedByRating();
                } else {
                    movies = database.movieDao().getAll();
                }

                ArrayList<Movie> result = new ArrayList<>();
                for (MovieWrapper movie : movies) {
                    result.add(MovieWrapper.toMovie(movie));
                }
                return result;
            }

            @Override
            protected void onPostExecute(ArrayList<Movie> movies) {
                super.onPostExecute(movies);
                if (movies == null) {
                    Snackbar.make(recyclerView, R.string.error_downloading_movie_list, Snackbar.LENGTH_LONG);
                    return;
                }

                pbLoading.setVisibility(View.GONE);
                if (movies.size() > 0) {
                    addMoviesToCache(movies);
                    adapter.setMovies(movies);
                    adapter.notifyDataSetChanged();
                    tvNotFound.setVisibility(View.GONE);
                } else {
                    adapter.clearMovies();
                    adapter.notifyDataSetChanged();
                    tvNotFound.setVisibility(View.VISIBLE);
                }
            }
        }).execute();
    }

    private void addMoviesToCache(ArrayList<Movie> results) {
        for (Movie movie : results) {
            cache.put(movie.id(), movie);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_movie_list_filter_title:
                if (this.sort.equals(SortBy.TITLE_ASC)) return;
                this.sort = SortBy.TITLE_ASC;
                resetList();
                tvTitleFilter.setBackgroundColor(getResources().getColor(R.color.selected_filter));
                tvTitleFilter.setTextColor(getResources().getColor(R.color.md_white_1000));
                break;
            case R.id.fragment_movie_list_filter_release:
                if (this.sort.equals(SortBy.RELEASE_ASC)) return;
                this.sort = SortBy.RELEASE_ASC;
                resetList();
                tvTitleRelease.setBackgroundColor(getResources().getColor(R.color.selected_filter));
                tvTitleRelease.setTextColor(getResources().getColor(R.color.md_white_1000));
                break;
            case R.id.fragment_movie_list_filter_rating:
                if (this.sort.equals(SortBy.RATING_DESC)) return;
                this.sort = SortBy.RATING_DESC;
                resetList();
                tvTitleRating.setBackgroundColor(getResources().getColor(R.color.selected_filter));
                tvTitleRating.setTextColor(getResources().getColor(R.color.md_white_1000));
                break;
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(getActivity(), MovieActivity.class);
        intent.putExtra(MovieActivity.ARG_MOVIE_ID, adapter.getMovie(position).id());
        startActivity(intent);
    }

    public void resetList() {
        adapter.clearMovies();
        adapter.notifyDataSetChanged();
        pbLoading.setVisibility(View.VISIBLE);
        loadMovieList();
        tvTitleFilter.setBackgroundColor(getResources().getColor(R.color.md_white_1000));
        tvTitleRelease.setBackgroundColor(getResources().getColor(R.color.md_white_1000));
        tvTitleRating.setBackgroundColor(getResources().getColor(R.color.md_white_1000));
        tvTitleFilter.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvTitleRelease.setTextColor(getResources().getColor(R.color.colorPrimary));
        tvTitleRating.setTextColor(getResources().getColor(R.color.colorPrimary));
    }
}