package me.yamilgarciahernandez.mdb;

import android.app.Application;
import android.util.Log;

import butterknife.ButterKnife;
import me.yamilgarciahernandez.mdb.di.module.CacheModule;
import me.yamilgarciahernandez.mdb.di.module.DatabaseModule;
import me.yamilgarciahernandez.mdb.di.module.NetworkModule;

public class MDB extends Application {

    NetworkModule networkModule;
    CacheModule cacheModule;
    DatabaseModule databaseModule;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) ButterKnife.setDebug(true);
        this.networkModule = new NetworkModule(BuildConfig.API_URL);
        this.databaseModule = new DatabaseModule(this);
        this.cacheModule = new CacheModule();
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    public NetworkModule getNetworkModule() {
        return networkModule;
    }

    public CacheModule getCacheModule() {
        return cacheModule;
    }

    public DatabaseModule getDatabaseModule() {
        return databaseModule;
    }
}
