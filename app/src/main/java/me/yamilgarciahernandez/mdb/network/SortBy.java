package me.yamilgarciahernandez.mdb.network;

public class SortBy {
    public static final String TITLE_ASC = "title.asc";
    public static final String TITLE_DESC = "title.desc";
    public static final String RELEASE_ASC = "release_date.asc";
    public static final String RELEASE_DESC = "release_date.desc";
    public static final String RATING_ASC = "vote_average.asc";
    public static final String RATING_DESC = "vote_average.desc";
}
