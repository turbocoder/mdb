package me.yamilgarciahernandez.mdb.network;

import me.yamilgarciahernandez.mdb.model.Movie;
import me.yamilgarciahernandez.mdb.model.api.MovieListResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TheMovieDBService {
    @GET("discover/movie?")
    Call<MovieListResponse> getMovieList(@Query("api_key") String key, @Query("sort_by") String sort, @Query("page") int page);
    
    @GET("movie/{id}")
    Call<Movie> getMovie(@Query("api_key") String key, @Path("id") int id);
}
