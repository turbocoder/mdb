package me.yamilgarciahernandez.mdb.network.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import org.greenrobot.eventbus.EventBus;

public class NetworkStateReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (!wifi.isAvailable() && !mobile.isAvailable()) {
            EventBus.getDefault().post(new NetworkStateChanged(false));
        }
    }

    public static class NetworkStateChanged {

        private boolean mIsInternetConnected;

        public NetworkStateChanged(boolean isInternetConnected) {
            this.mIsInternetConnected = isInternetConnected;
        }

        public boolean isInternetConnected() {
            return this.mIsInternetConnected;
        }
    }
}