package me.yamilgarciahernandez.mdb.listener;

import android.util.Log;

import me.yamilgarciahernandez.mdb.BuildConfig;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ServiceListener<Result> implements Callback<Result> {

    private static final int TOTAL_RETRIES = 3;
    private final Call<Result> call;
    private int retryCount = 0;

    public ServiceListener(Call<Result> call) {
        this.call = call;
    }

    @Override
    public void onResponse(Call<Result> call, Response<Result> response) {
        try {
            if (response == null) {
                error(new Exception("Not Response Received"));
                return;
            }

            if (response.body() == null) {
                error(new Exception("Empty Response Received"));
                return;
            }

            success(response.body());
        } catch (Exception ex) {
            error(ex);
        }
    }

    @Override
    public void onFailure(Call<Result> call, Throwable t) {
        Log.e(BuildConfig.LOG_TAG, "Exception", t);
        if (retryCount++ < TOTAL_RETRIES) {
            Log.d(BuildConfig.LOG_TAG, "Retrying... (" + retryCount + " out of " + TOTAL_RETRIES + ")");
            retry();
        } else {
            error(new Exception(t));
        }
    }

    private void retry() {
        call.clone().enqueue(this);
    }

    public abstract void success(Result result) throws Exception;

    public abstract void error(Exception ex);
}
