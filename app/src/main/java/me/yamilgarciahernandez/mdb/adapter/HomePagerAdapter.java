package me.yamilgarciahernandez.mdb.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.ui.fragment.FavoriteListFragment;
import me.yamilgarciahernandez.mdb.ui.fragment.MovieListFragment;

public class HomePagerAdapter extends FragmentStatePagerAdapter {
    private Fragment[] fragments = new Fragment[]{
            MovieListFragment.getInstance(),
            FavoriteListFragment.getInstance()
    };

    public HomePagerAdapter(FragmentManager fm) {
        super(fm);
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    @Override
    public Fragment getItem(int i) {
        return fragments[i];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

}