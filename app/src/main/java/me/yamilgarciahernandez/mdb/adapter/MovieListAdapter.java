package me.yamilgarciahernandez.mdb.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.R;
import me.yamilgarciahernandez.mdb.model.Movie;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.ViewHolder> {

    private ArrayList<Movie> movies;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public MovieListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.movies = new ArrayList();
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_movie, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Movie movie = movies.get(position);
        holder.title.setText(movie.title());
        Picasso.get()
                .load(BuildConfig.CDN_URL + movie.image())
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .transform(new RoundedTransformation(4, 2))
                .fit()
                .centerCrop()
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        ImageView image;

        ViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.item_movie_title);
            this.image = (ImageView) itemView.findViewById(R.id.item_movie_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public Movie getMovie(int index) {
        return movies.get(index);
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public void addMovies(ArrayList<Movie> movies) {
        this.movies.addAll(movies);
    }

    public void clearMovies() {
        movies.clear();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
