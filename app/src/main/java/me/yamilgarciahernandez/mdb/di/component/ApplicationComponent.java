package me.yamilgarciahernandez.mdb.di.component;

import android.app.Application;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Component(modules = {AndroidInjectionModule.class})
public interface ApplicationComponent {
    void inject(Application app);
}
