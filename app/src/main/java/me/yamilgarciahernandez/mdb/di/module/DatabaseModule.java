package me.yamilgarciahernandez.mdb.di.module;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.util.Log;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import dagger.Module;
import dagger.Provides;
import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.database.MovieDatabase;
import me.yamilgarciahernandez.mdb.model.Movie;

@Module
public class DatabaseModule {

    private MovieDatabase database;

    public DatabaseModule(Context ctx) {
        database = Room.databaseBuilder(ctx, MovieDatabase.class, "MDB").build();
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    @Provides
    MovieDatabase provideMovieDatabase() {
        return database;
    }
}
