package me.yamilgarciahernandez.mdb.di.component;

import dagger.Component;
import me.yamilgarciahernandez.mdb.di.module.HomePageModule;
import me.yamilgarciahernandez.mdb.ui.activity.HomeActivity;

@Component(modules = {HomePageModule.class})
public interface HomePageComponent {
    void inject(HomeActivity fragment);
}
