package me.yamilgarciahernandez.mdb.di.component;

import dagger.Component;
import me.yamilgarciahernandez.mdb.di.module.CacheModule;
import me.yamilgarciahernandez.mdb.di.module.NetworkModule;
import me.yamilgarciahernandez.mdb.ui.fragment.MovieListFragment;
import me.yamilgarciahernandez.mdb.di.module.MovieListModule;

@Component(modules = {MovieListModule.class, NetworkModule.class, CacheModule.class})
public interface MovieListComponent {
    void inject(MovieListFragment fragment);
}
