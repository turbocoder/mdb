package me.yamilgarciahernandez.mdb.di.module;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.adapter.MovieListAdapter;
import me.yamilgarciahernandez.mdb.listener.EndlessScrollListener;

@Module
public class MovieListModule {

    private Context context;
    private int columns;

    public MovieListModule(Context context, int columns) {
        this.context = context;
        this.columns = columns;
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    @Provides
    MovieListAdapter provideMovieListAdapter() {
        return new MovieListAdapter(context);
    }

    @Provides
    GridLayoutManager provideGridLayoutManager() {
        return new GridLayoutManager(context, columns);
    }
}
