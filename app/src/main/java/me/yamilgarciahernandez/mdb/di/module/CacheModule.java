package me.yamilgarciahernandez.mdb.di.module;

import android.util.Log;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import dagger.Module;
import dagger.Provides;
import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.model.Movie;

@Module
public class CacheModule {

    private Cache<Integer, Movie> cachedMovies;

    public CacheModule() {
        cachedMovies = CacheBuilder.newBuilder()
                .build();
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    @Provides
    Cache<Integer, Movie> provideCachedMovies() {
        return cachedMovies;
    }
}
