package me.yamilgarciahernandez.mdb.di.component;

import dagger.Component;
import me.yamilgarciahernandez.mdb.di.module.CacheModule;
import me.yamilgarciahernandez.mdb.di.module.DatabaseModule;
import me.yamilgarciahernandez.mdb.ui.activity.MovieActivity;

@Component(modules = {DatabaseModule.class, CacheModule.class})
public interface MovieSingleComponent {
    void inject(MovieActivity activity);
}
