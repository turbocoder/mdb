package me.yamilgarciahernandez.mdb.di.module;

import android.support.v4.app.FragmentManager;
import android.util.Log;

import dagger.Module;
import dagger.Provides;
import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.adapter.HomePagerAdapter;

@Module
public class HomePageModule {

    private FragmentManager manager;

    public HomePageModule(FragmentManager manager) {
        this.manager = manager;
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    @Provides
    HomePagerAdapter provideHomePageAdapter() {
        return new HomePagerAdapter(manager);
    }
}
