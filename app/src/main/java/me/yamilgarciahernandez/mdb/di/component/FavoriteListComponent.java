package me.yamilgarciahernandez.mdb.di.component;

import dagger.Component;
import me.yamilgarciahernandez.mdb.di.module.CacheModule;
import me.yamilgarciahernandez.mdb.di.module.DatabaseModule;
import me.yamilgarciahernandez.mdb.di.module.MovieListModule;
import me.yamilgarciahernandez.mdb.di.module.NetworkModule;
import me.yamilgarciahernandez.mdb.ui.fragment.FavoriteListFragment;
import me.yamilgarciahernandez.mdb.ui.fragment.MovieListFragment;

@Component(modules = {MovieListModule.class, DatabaseModule.class, CacheModule.class})
public interface FavoriteListComponent {
    void inject(FavoriteListFragment fragment);
}
