package me.yamilgarciahernandez.mdb.di.module;

import android.util.Log;

import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import me.yamilgarciahernandez.mdb.BuildConfig;
import me.yamilgarciahernandez.mdb.factory.AutoValueGsonFactory;
import me.yamilgarciahernandez.mdb.network.TheMovieDBService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private TheMovieDBService service;

    public NetworkModule(String url) {
        GsonConverterFactory gsonConverterFactory = GsonConverterFactory.create(
                new GsonBuilder().registerTypeAdapterFactory(AutoValueGsonFactory.create())
                        .create());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(gsonConverterFactory)
                .build();

        service = retrofit.create(TheMovieDBService.class);
        Log.d(BuildConfig.LOG_TAG, this.getClass().getName() + " has been initialized");
    }

    @Provides
    TheMovieDBService provideTheMovieDBService() {
        return service;
    }
}
