package me.yamilgarciahernandez.mdb.model.api;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import me.yamilgarciahernandez.mdb.model.Movie;

@AutoValue
public abstract class MovieListResponse {

    @SerializedName("page")
    public abstract int page();

    @SerializedName("total_pages")
    public abstract int totalPages();

    @SerializedName("total_result")
    public abstract int totalResults();

    @SerializedName("results")
    public abstract ArrayList<Movie> results();

    public static TypeAdapter<MovieListResponse> typeAdapter(Gson gson) {
        return new AutoValue_MovieListResponse.GsonTypeAdapter(gson);
    }
}