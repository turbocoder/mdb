package me.yamilgarciahernandez.mdb.model;

import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Nullable;


@AutoValue
public abstract class Movie {

    public static Movie create(int id, String title, String overview, String image, double rating, String release) {
        return new AutoValue_Movie(id, title, overview, image, rating, release);
    }

    @SerializedName("id")
    public abstract int id();

    @SerializedName("title")
    public abstract String title();

    @SerializedName("overview")
    public abstract String overview();

    @SerializedName("poster_path")
    @Nullable
    public abstract String image();

    @SerializedName("vote_average")
    public abstract double rating();

    @SerializedName("release_date")
    public abstract String release();

    public static TypeAdapter<Movie> typeAdapter(Gson gson) {
        return new AutoValue_Movie.GsonTypeAdapter(gson);
    }
}