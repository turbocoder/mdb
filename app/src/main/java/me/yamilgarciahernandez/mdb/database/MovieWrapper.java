package me.yamilgarciahernandez.mdb.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import me.yamilgarciahernandez.mdb.model.Movie;

@Entity(tableName = "movie")
public class MovieWrapper {

    public MovieWrapper() {
    }

    public MovieWrapper(int id, String title, String overview, String image, double rating, String release) {
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.image = image;
        this.rating = rating;
        this.release = release;
    }

    @PrimaryKey
    public int id;

    @ColumnInfo(name = "title")
    public String title;

    @ColumnInfo(name = "overview")
    public String overview;

    @ColumnInfo(name = "image")
    public String image;

    @ColumnInfo(name = "rating")
    public double rating;

    @ColumnInfo(name = "release")
    public String release;

    public static Movie toMovie(MovieWrapper wrapper) {
        if (wrapper == null) return null;
        return Movie.create(wrapper.id, wrapper.title, wrapper.overview, wrapper.image, wrapper.rating, wrapper.release);
    }

    public static MovieWrapper fromMovie(final Movie movie) {
        if (movie == null) return null;
        return new MovieWrapper(movie.id(), movie.title(), movie.overview(), movie.image(), movie.rating(), movie.release());
    }
}
