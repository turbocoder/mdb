package me.yamilgarciahernandez.mdb.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MovieDao {
    @Query("SELECT * FROM movie")
    List<MovieWrapper> getAll();

    @Query("SELECT * FROM movie ORDER BY title ASC")
    List<MovieWrapper> getAllOrderedByTitle();

    @Query("SELECT * FROM movie ORDER BY 'release' ASC")
    List<MovieWrapper> getAllOrderedByRelease();

    @Query("SELECT * FROM movie ORDER BY rating DESC")
    List<MovieWrapper> getAllOrderedByRating();

    @Query("SELECT * FROM movie WHERE id = :id LIMIT 1")
    MovieWrapper findById(int id);

    @Query("DELETE FROM movie WHERE id = :id")
    void deleteById(int id);

    @Insert
    void insertAll(MovieWrapper... movies);

    @Delete
    void delete(MovieWrapper movie);
}
