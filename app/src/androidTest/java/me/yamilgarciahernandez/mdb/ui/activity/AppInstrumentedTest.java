package me.yamilgarciahernandez.mdb.ui.activity;


import android.os.SystemClock;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import me.yamilgarciahernandez.mdb.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AppInstrumentedTest {

    private final int DELAY = 1500;
    @Rule
    public ActivityTestRule<HomeActivity> mActivityTestRule = new ActivityTestRule<>(HomeActivity.class);

    @Test
    public void appInstrumentedTest() {
        SystemClock.sleep(DELAY);
        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.fragment_movie_list_rv),
                        isDisplayed(),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                3)));
        recyclerView.perform(actionOnItemAtPosition(1, click()));

        SystemClock.sleep(DELAY);
        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.activity_movie_fab),
                        isDisplayed(),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        floatingActionButton.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        isDisplayed(),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withId(R.id.toolbar_layout),
                                                1)),
                                1),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction tabView = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.activity_home_tabs),
                                0),
                        1),
                        isDisplayed()));
        tabView.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction viewPager = onView(
                allOf(withId(R.id.activity_home_tab_container),
                        isDisplayed(),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        viewPager.perform(swipeLeft());

        SystemClock.sleep(DELAY);
        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.fragment_movie_list_rv),
                        isDisplayed(),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                3)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

        SystemClock.sleep(DELAY);
        ViewInteraction floatingActionButton2 = onView(
                allOf(withId(R.id.activity_movie_fab),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        floatingActionButton2.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withId(R.id.toolbar_layout),
                                                1)),
                                1),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction tabView2 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.activity_home_tabs),
                                0),
                        0),
                        isDisplayed()));
        tabView2.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction viewPager2 = onView(
                allOf(withId(R.id.activity_home_tab_container),
                        isDisplayed(),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        viewPager2.perform(swipeRight());

        SystemClock.sleep(DELAY);
        ViewInteraction iconicsTextView = onView(
                allOf(withId(R.id.fragment_movie_list_filter_rating),
                        isDisplayed(), withText("\uF005 Rating"),
                        childAtPosition(
                                allOf(withId(R.id.fragment_movie_list_filter_container),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                2)),
                                2),
                        isDisplayed()));
        iconicsTextView.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction recyclerView3 = onView(
                allOf(withId(R.id.fragment_movie_list_rv),
                        isDisplayed(),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                3)));
        recyclerView3.perform(actionOnItemAtPosition(0, click()));

        SystemClock.sleep(DELAY);
        ViewInteraction floatingActionButton3 = onView(
                allOf(withId(R.id.activity_movie_fab),
                        isDisplayed(),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        floatingActionButton3.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction appCompatImageButton3 = onView(
                allOf(withContentDescription("Navigate up"),
                        isDisplayed(),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        isDisplayed(),
                                        childAtPosition(
                                                withId(R.id.toolbar_layout),
                                                1)),
                                1),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction iconicsTextView2 = onView(
                allOf(withId(R.id.fragment_movie_list_filter_title),
                        isDisplayed(), withText("\uF0CB Title"),
                        childAtPosition(
                                allOf(withId(R.id.fragment_movie_list_filter_container),
                                        childAtPosition(
                                                withClassName(is("android.widget.RelativeLayout")),
                                                2)),
                                0),
                        isDisplayed()));
        iconicsTextView2.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction recyclerView4 = onView(
                allOf(withId(R.id.fragment_movie_list_rv),
                        isDisplayed(),
                        childAtPosition(
                                withClassName(is("android.widget.RelativeLayout")),
                                3)));
        recyclerView4.perform(actionOnItemAtPosition(3, click()));

        SystemClock.sleep(DELAY);
        ViewInteraction floatingActionButton4 = onView(
                allOf(withId(R.id.activity_movie_fab),
                        isDisplayed(),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        floatingActionButton4.perform(click());

        SystemClock.sleep(DELAY);
        pressBack();

        SystemClock.sleep(DELAY);
        ViewInteraction tabView3 = onView(
                allOf(childAtPosition(
                        childAtPosition(
                                withId(R.id.activity_home_tabs),
                                0),
                        1),
                        isDisplayed()));
        tabView3.perform(click());

        SystemClock.sleep(DELAY);
        ViewInteraction viewPager3 = onView(
                allOf(withId(R.id.activity_home_tab_container),
                        isDisplayed(),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        viewPager3.perform(swipeLeft());

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
