package me.yamilgarciahernandez.mdb.database;

import org.junit.Test;

import me.yamilgarciahernandez.mdb.model.Movie;

import static org.junit.Assert.assertEquals;

public class MovieWrapperUnitTest {

    @Test
    public void createIsCorrect() {
        MovieWrapper movieWrapper = new MovieWrapper();
        movieWrapper.id = 1;
        movieWrapper.title = "title #1";
        movieWrapper.overview = "overview #1";
        movieWrapper.image = "image #1";
        movieWrapper.rating = 10.0;
        movieWrapper.release = "2018-07-22";

        assertEquals(1, movieWrapper.id);
        assertEquals("title #1", movieWrapper.title);
        assertEquals("overview #1", movieWrapper.overview);
        assertEquals("image #1", movieWrapper.image);
        assertEquals(10.0, movieWrapper.rating, 1e-15);
        assertEquals("2018-07-22", movieWrapper.release);
    }

    @Test
    public void toMovieIsCorrect() {
        MovieWrapper movieWrapper = new MovieWrapper();
        movieWrapper.id = 1;
        movieWrapper.title = "title #1";
        movieWrapper.overview = "overview #1";
        movieWrapper.image = "image #1";
        movieWrapper.rating = 10.0;
        movieWrapper.release = "2018-07-22";

        Movie movie = MovieWrapper.toMovie(movieWrapper);

        assertEquals(movieWrapper.id, movie.id());
        assertEquals(movieWrapper.title, movie.title());
        assertEquals(movieWrapper.overview, movie.overview());
        assertEquals(movieWrapper.image, movie.image());
        assertEquals(movieWrapper.rating, movie.rating(), 1e-15);
        assertEquals(movieWrapper.release, movie.release());
    }

    @Test
    public void fromMovieIsCorrect() {
        Movie movie = Movie.create(1, "title #1", "overview #1", "image #1", 10.0, "2018-07-22");
        MovieWrapper movieWrapper = MovieWrapper.fromMovie(movie);

        assertEquals(movie.id(), movieWrapper.id);
        assertEquals(movie.title(), movieWrapper.title);
        assertEquals(movie.overview(), movieWrapper.overview);
        assertEquals(movie.image(), movieWrapper.image);
        assertEquals(movie.rating(), movieWrapper.rating, 1e-15);
        assertEquals(movie.release(), movieWrapper.release);
    }
}
