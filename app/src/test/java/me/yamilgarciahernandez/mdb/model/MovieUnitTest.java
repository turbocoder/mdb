package me.yamilgarciahernandez.mdb.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MovieUnitTest {

    @Test
    public void createIsCorrect() {
        Movie movie = Movie.create(1, "title #1", "overview #1", "image #1", 10.0, "2018-07-22");
        assertEquals(1, movie.id());
        assertEquals("title #1", movie.title());
        assertEquals("overview #1", movie.overview());
        assertEquals("image #1", movie.image());
        assertEquals(10.0, movie.rating(), 1e-15);
        assertEquals("2018-07-22", movie.release());
    }
}
